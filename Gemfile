# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '6.0.1'
# Use postgres as the database for Active Record
gem 'pg', '0.18.4'
# Use Devise for authentification
gem 'devise', '4.7.1'
gem 'omniauth', '1.9.0'
gem 'omniauth-facebook', '5.0.0'
# Use Puma as the app server
gem 'puma', '4.3.1'
# Transpile app-like JavaScript and [S]CSS. Read more: https://github.com/rails/webpacker
gem 'webpacker', '4.2.2'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '5.2.1'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '2.9.1'
# Use Redis adapter to run Action Cable in production
gem 'redis', '4.1.3'
# Use Active Model has_secure_password
# Might not be useful with devise, because it can be a dependancie ?
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '1.4.5', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', '11.0.1', platforms: %i[mri mingw x64_mingw]
  gem 'dotenv-rails', '2.7.5'
  gem 'rubocop', '0.77.0', require: false
  gem 'brakeman', '4.7.2', require: false
end

group :development do
  gem 'listen', '3.1.5'
  gem 'solargraph', '0.38.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring', '2.1.0'
  gem 'spring-watcher-listen', '2.0.1'
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '4.0.1'
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara', '3.29.0'
  gem 'minitest-reporters', '1.4.2'
  gem 'selenium-webdriver', '3.142.6'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers', '4.1.3'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', '1.2019.3', platforms: %i[mingw mswin x64_mingw jruby]
