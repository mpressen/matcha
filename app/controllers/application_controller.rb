# frozen_string_literal: true

# rubocop:todo Style/Documentation
class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_user!, except: %i[home politique_confidentialite]

  protected

  def configure_permitted_parameters
    # https://github.com/rails/strong_parameters#nested-parameters
    # devise_parameter_sanitizer.permit(:account_update) do |user_params|
    #   user_params.permit({ roles: [] }, :email, :password, :password_confirmation)
    # end
  end
end
# rubocop:enable Style/Documentation
