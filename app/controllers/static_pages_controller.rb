# frozen_string_literal: true

# rubocop:todo Style/Documentation
class StaticPagesController < ApplicationController
  def home; end

  def politique_confidentialite; end
end
# rubocop:enable Style/Documentation
