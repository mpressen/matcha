# frozen_string_literal: true

module ApplicationHelper # rubocop:todo Style/Documentation
  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = 'Matcha'
    if page_title.present?
      "#{page_title} | #{base_title}"
    else
      base_title
    end
  end
end
