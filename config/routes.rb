# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' } do
    delete 'sign_out', to: 'devise/sessions#destroy', as: :destroy_user_session
  end
  root 'static_pages#home'
  get '/politique-confidentialite', to: 'static_pages#politique_confidentialite'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
