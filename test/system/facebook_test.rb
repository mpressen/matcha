# frozen_string_literal: true

require 'application_system_test_case'

class FacebookTest < ApplicationSystemTestCase
  test 'user can sign up with facebook, log out and log in' do
    Capybara.server_port = 4000
    # sign up
    visit root_path
    skip 'not testing facebook auth' unless page.current_url.match '4000'
    click_link 'Sign up'
    click_link 'Sign in with Facebook'
    sleep 1
    fill_in 'email', with: ENV.fetch('FCBK_LOGIN')
    fill_in 'pass', with: ENV.fetch('FCBK_PWD')
    click_button 'login'
    sleep 1
    assert_selector '.alert-notice'
    assert_text 'Successfully authenticated from Facebook account.'
    # log out
    click_link 'Log out'
    assert_selector '.alert-notice'
    assert_text 'Signed out'
    # log in
    click_link 'Log in'
    click_link 'Sign in with Facebook'
    sleep 1
    assert_selector '.alert-notice'
    assert_text 'Successfully authenticated from Facebook account.'
  end
end
