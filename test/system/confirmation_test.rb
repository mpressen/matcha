# frozen_string_literal: true

require 'application_system_test_case'

class ConfirmationTest < ApplicationSystemTestCase
  test "clicking confirmation email's link redirects to log in" do
    sign_up(email: 'foobar@foo.bar', confirmed: false)
    user = User.find_by(email: 'foobar@foo.bar')

    visit user_confirmation_path(email: user.email,
                                 confirmation_token: user.confirmation_token)

    assert_current_path new_user_session_path
    assert_selector '.alert-notice'
    assert_text 'confirmed'
  end

  test 'resend confirmation instructions' do
    email = 'foobar@foo.bar'

    sign_up(email: email, confirmed: false)
    ActionMailer::Base.deliveries.clear

    visit new_user_registration_path
    click_link "Didn't receive confirmation instructions?"

    assert_current_path new_user_confirmation_path

    fill_in 'user[email]', with: email
    click_button

    # send appropriate email
    mails = ActionMailer::Base.deliveries
    assert_equal 1, mails.size
    mail = mails.first
    assert_equal 'Confirmation instructions', mail.subject
    assert_equal email, mail.to.first

    # display proper flash message
    assert_selector '.alert-notice'
    assert_text 'confirm'
  end

  test "don't send mail to a non user" do
    visit new_user_registration_path
    fill_in 'user[email]', with: 'foobar@foo.bar'
    click_button

    assert_selector '#error_explanation'
  end
end
