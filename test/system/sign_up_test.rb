# frozen_string_literal: true

require 'application_system_test_case'

class SignUpTest < ApplicationSystemTestCase
  test "new user can't sign up with input errors" do
    # no email
    sign_up(email: '', password: 'foobar', confirmed: false)

    assert_selector '#error_explanation'

    # no password
    sign_up(email: 'foobar@foo.bar', password: '', confirmed: false)

    assert_selector '#error_explanation'

    # no password confirmation
    visit new_user_registration_path
    fill_in 'user[email]', with: 'foobar@foo.bar'
    fill_in 'user[password]', with: 'foobar'
    fill_in 'user[password_confirmation]', with: ''
    click_button 'Sign up'

    assert_selector '#error_explanation'
  end

  test 'new user can sign up' do
    ActionMailer::Base.deliveries.clear
    user_email = 'foobar@foo.bar'

    # create a new user, unconfirmed
    visit root_path
    click_link 'Sign up now !'
    fill_in 'user[email]', with: user_email
    fill_in 'user[password]', with: 'foobar'
    fill_in 'user[password_confirmation]', with: 'foobar'
    click_button 'Sign up'

    # send appropriate email
    mails = ActionMailer::Base.deliveries
    assert_equal 1, mails.size
    mail = mails.first
    assert_equal 'Confirmation instructions', mail.subject
    assert_equal user_email, mail.to.first

    # display proper flash message
    assert_selector '.alert-notice'
    assert_text 'confirmation'
  end

  test "user can't sign up again" do
    sign_up(email: 'foobar@foo.bar', password: 'foobar', confirmed: false)
    sign_up(email: 'foobar@foo.bar', password: 'foobar', confirmed: false)

    assert_selector '#error_explanation'
  end
end
