# frozen_string_literal: true

require 'application_system_test_case'

class LogInTest < ApplicationSystemTestCase
  test 'user confirmed can log in' do
    sign_up(email: 'foobar@foo.bar', password: 'foobar')
    visit root_path
    click_link 'Log in'
    fill_in 'user[email]', with: 'foobar@foo.bar'
    fill_in 'user[password]', with: 'foobar'
    click_button

    assert_selector '.alert-notice'
    assert_text 'Signed in'
  end

  test "user not confirmed can't log in" do
    sign_up(confirmed: false)
    log_in

    assert_current_path new_user_session_path
    assert_selector '.alert-alert'
    assert_text 'confirm'
  end

  test "wrong user can't log in" do
    log_in

    assert_current_path new_user_session_path
    assert_selector '.alert-alert'
    assert_text 'Invalid'
  end
end
