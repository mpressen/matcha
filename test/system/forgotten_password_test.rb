# frozen_string_literal: true

require 'application_system_test_case'

class ForgottenPasswordTest < ApplicationSystemTestCase
  test "don't send mail to a non user" do
    visit new_user_password_path
    fill_in 'user[email]', with: 'foobar@foo.bar'
    click_button

    assert_selector '#error_explanation'
  end

  test 'send mail' do
    email = 'foobar@foo.bar'
    sign_up(email: email, confirmed: false)
    ActionMailer::Base.deliveries.clear

    visit new_user_session_path
    click_link 'Forgot your password?'

    assert_current_path new_user_password_path

    fill_in 'user[email]', with: email
    click_button

    # send appropriate email
    mails = ActionMailer::Base.deliveries
    assert_equal 1, mails.size
    mail = mails.first
    assert_equal 'Reset password instructions', mail.subject
    assert_equal email, mail.to.first

    # display proper flash message
    assert_selector '.alert-notice'
    assert_text 'reset your password'
  end
end
