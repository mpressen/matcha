# frozen_string_literal: true

require 'application_system_test_case'

class LogOutsTest < ApplicationSystemTestCase
  test 'user logged in can logged out' do
    sign_up
    log_in
    click_link 'Log out'

    assert_selector '.alert-notice'
    assert_text 'Signed out'
  end
end
