# frozen_string_literal: true

require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
  test 'should get home as unlogged user' do
    get root_path
    assert_response :success
    assert_select 'a[href=?]', root_path, text: 'MATCHA'
    assert_select 'a[href=?]', new_user_registration_path, text: 'Sign up now !'
    assert_select 'a[href=?]', new_user_session_path, text: 'Log in'
    assert_select 'a[href=?]', 'https://mpressen.github.io/',
                  text: 'Maximilien Pressensé'
  end

  test 'should get politique_confidentialite as unlogged user' do
    get politique_confidentialite_path
    assert_response :success
  end
end
