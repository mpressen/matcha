# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
require 'minitest/reporters'
Minitest::Reporters.use!

require 'capybara/rails'
require 'capybara/minitest'

Capybara.server = :puma, {
  Host: "ssl://#{Capybara.server_host}?key=#{ENV.fetch('SSL_KEY_PATH')}&cert=#{ENV.fetch('SSL_CERT_PATH')}"
}

class ActiveSupport::TestCase
  # Run tests in parallel with specified workers
  parallelize(workers: :number_of_processors)

  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...

  include Devise::Test::IntegrationHelpers
  # sign_in users(:bob)
  # sign_in users(:bob), scope: :admin
  # sign_out :user
end
