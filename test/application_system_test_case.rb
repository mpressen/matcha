# frozen_string_literal: true

require 'test_helper'

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  driven_by :selenium, using: :headless_chrome

  # to allow system testing throw https
  def setup
    Capybara.app_host = 'https://localhost'
  end

  def sign_up(email: 'foobar@foo.bar', password: 'foobar', confirmed: true)
    visit new_user_registration_path
    fill_in 'user[email]', with: email
    fill_in 'user[password]', with: password
    fill_in 'user[password_confirmation]', with: password
    click_button

    return unless confirmed

    user = User.find_by(email: 'foobar@foo.bar')
    visit user_confirmation_path(email: user.email,
                                 confirmation_token: user.confirmation_token)
  end

  def log_in(email: 'foobar@foo.bar', password: 'foobar')
    visit new_user_session_path
    fill_in 'user[email]', with: email
    fill_in 'user[password]', with: password
    click_button
  end
end
